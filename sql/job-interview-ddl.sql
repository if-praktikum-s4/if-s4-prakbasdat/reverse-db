CREATE TABLE `users` (
  `user_id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) UNIQUE NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_picture` varchar(255)
);

CREATE TABLE `pins` (
  `pin_id` int PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `image_url` varchar(255) NOT NULL,
  `user_id` int NOT NULL
);

CREATE TABLE `boards` (
  `board_id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `user_id` int NOT NULL
);

CREATE TABLE `categories` (
  `category_id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(50) UNIQUE NOT NULL
);

CREATE TABLE `pinCategory` (
  `pin_id` int NOT NULL,
  `category_id` int NOT NULL,
  PRIMARY KEY (`pin_id`, `category_id`)
);

CREATE TABLE `comments` (
  `comment_id` int PRIMARY KEY AUTO_INCREMENT,
  `text` text NOT NULL,
  `pin_id` int NOT NULL,
  `user_id` int NOT NULL,
  `date_created` timestamp DEFAULT (CURRENT_TIMESTAMP)
);

CREATE TABLE `pinLikes` (
  `like_id` int PRIMARY KEY AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `pin_id` int NOT NULL,
  `date_created` timestamp DEFAULT (CURRENT_TIMESTAMP)
);

CREATE TABLE `follows` (
  `follow_id` int PRIMARY KEY AUTO_INCREMENT,
  `follower_user_id` int NOT NULL,
  `following_user_id` int NOT NULL,
  `date_created` timestamp DEFAULT (CURRENT_TIMESTAMP)
);

CREATE TABLE `notifications` (
  `notification_id` int PRIMARY KEY AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `message` text NOT NULL,
  `date_created` timestamp DEFAULT (CURRENT_TIMESTAMP),
  `is_read` boolean DEFAULT false
);

CREATE TABLE `boardPin` (
  `board_id` int NOT NULL,
  `pin_id` int NOT NULL,
  `date_created` timestamp DEFAULT (CURRENT_TIMESTAMP),
  PRIMARY KEY (`board_id`, `pin_id`)
);

CREATE TABLE `conversations` (
  `conversation_id` int PRIMARY KEY AUTO_INCREMENT,
  `user1_id` int NOT NULL,
  `user2_id` int NOT NULL,
  `date_created` timestamp DEFAULT (CURRENT_TIMESTAMP)
);

CREATE TABLE `messages` (
  `message_id` int PRIMARY KEY AUTO_INCREMENT,
  `text` text NOT NULL,
  `sender_id` int NOT NULL,
  `receiver_id` int NOT NULL,
  `conversation_id` int NOT NULL,
  `is_read` boolean DEFAULT false,
  `date_read` timestamp,
  `date_sent` timestamp DEFAULT (CURRENT_TIMESTAMP)
);

ALTER TABLE `pins` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `boards` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `pinCategory` ADD FOREIGN KEY (`pin_id`) REFERENCES `pins` (`pin_id`);

ALTER TABLE `pinCategory` ADD FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`);

ALTER TABLE `comments` ADD FOREIGN KEY (`pin_id`) REFERENCES `pins` (`pin_id`);

ALTER TABLE `comments` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `pinLikes` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `pinLikes` ADD FOREIGN KEY (`pin_id`) REFERENCES `pins` (`pin_id`);

ALTER TABLE `follows` ADD FOREIGN KEY (`follower_user_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `follows` ADD FOREIGN KEY (`following_user_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `notifications` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `boardPin` ADD FOREIGN KEY (`board_id`) REFERENCES `boards` (`board_id`);

ALTER TABLE `boardPin` ADD FOREIGN KEY (`pin_id`) REFERENCES `pins` (`pin_id`);

ALTER TABLE `conversations` ADD FOREIGN KEY (`user1_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `conversations` ADD FOREIGN KEY (`user2_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `messages` ADD FOREIGN KEY (`sender_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `messages` ADD FOREIGN KEY (`receiver_id`) REFERENCES `users` (`user_id`);

ALTER TABLE `messages` ADD FOREIGN KEY (`conversation_id`) REFERENCES `conversations` (`conversation_id`);
