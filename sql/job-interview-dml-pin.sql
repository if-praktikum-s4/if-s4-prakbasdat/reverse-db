INSERT into pin (title, description, image_url, user_id)
VALUES
('ootd', 'Mix and match your favorite pieces to create your own unique style!', 'https://www.pinterest.com/pin/876795239731516209/', 1),
('running', 'Get motivated to run with these inspiring quotes!', 'https://www.pinterest.com/pin/876795239731516211/', 2),
('pose idea', 'Strike a pose and capture the moment!', 'https://www.pinterest.com/pin/876795239731516210/', 3),
('quotes', 'Words of wisdom to live by!', 'https://www.pinterest.com/pin/876795239731516213/', 4),
('cat', 'Cute and cuddly feline friends!', 'https://www.pinterest.com/pin/876795239731516212/', 5),
('acoustic', 'Unwind with these acoustic guitar melodies.', 'https://www.pinterest.com/pin/876795239731516214/', 1),
('ootd', 'Dress up or dress down, it’s up to you!', 'https://www.pinterest.com/pin/876795239731516215/', 2),
('running', 'Push yourself to go the extra mile!', 'https://www.pinterest.com/pin/876795239731516216/', 3),
('pose idea', 'Get creative with your poses and make your photos stand out!', 'https://www.pinterest.com/pin/876795239731516217/', 4),
('quotes', 'Find inspiration in these wise words!', 'https://www.pinterest.com/pin/876795239731516218/', 5),
('cat', 'Adorable furry friends that will steal your heart!', 'https://www.pinterest.com/pin/876795239731516219/', 1),
('acoustic', 'Enjoy the soothing sounds of acoustic music!', 'https://www.pinterest.com/pin/876795239731516220/', 2),
('ootd', 'Take fashion to the next level with these stylish outfit ideas!', 'https://www.pinterest.com/pin/876795239731516221/', 3),
('running', 'Stay motivated and stay on track!', 'https://www.pinterest.com/pin/876795239731516222/', 4),
('pose idea', 'Capture the perfect moment with these photo poses!', 'https://www.pinterest.com/pin/876795239731516223/', 5),
('quotes', 'Words of wisdom to inspire and uplift!', 'https://www.pinterest.com/pin/876795239731516224/', 1),
('cat', 'Purr-fectly adorable kitties to brighten your day!', 'https://www.pinterest.com/pin/876795239731516225/', 2),
('acoustic', 'Relax and unwind with the soothing sounds of acoustic music!', 'https://www.pinterest.com/pin/876795239731516226/', 3),
('ootd', 'Fashion inspiration to help you step up your style game!', 'https://www.pinterest.com/pin/876795239731516227/', 4),
('running', 'Find the motivation to keep going!', 'https://www.pinterest.com/pin/876795239731516228/', 5),
('pose idea', 'Take your photos to the next level with these creative pose ideas!', 'https://www.pinterest.com/pin/876795239731516229/', 1),
('quotes', 'Inspirational quotes to help you stay positive and', 'pinterest.com', 4);

INSERT into pin (title, description, image_url, user_id)
VALUES
('ootd', 'Feeling cute today', 'https://i.pinimg.com/564x/18/15/7d/18157d16e0e3c4ba79b72d98c06ad0d7.jpg', 2),
('acoustic', 'Love this song', 'https://i.pinimg.com/564x/5b/45/20/5b45201db6b158a6ce00e6f3278f8abf.jpg', 3),
('quotes', 'Stay positive', 'https://i.pinimg.com/564x/27/9f/d6/279fd6b7d45c1fa6d7cb88ec7f469bc5.jpg', 4),
('pose idea', 'Trying something new', 'https://i.pinimg.com/564x/36/f7/54/36f7542b0c20b79f536e8af97c073090.jpg', 5),
('running', 'Feeling the burn', 'https://i.pinimg.com/564x/cb/f6/e8/cbf6e8e77ab3ba939b8f3c9aa9edc6a9.jpg', 1),
('cat', 'Meet my new fur baby', 'https://i.pinimg.com/564x/9d/bd/9a/9dbd9a981d188b10c73edf6db7b6a007.jpg', 2),
('quotes', 'Believe in yourself', 'https://i.pinimg.com/564x/5e/0c/33/5e0c33c8465c5dd8e5b97d98b39f692c.jpg', 3),
('ootd', 'Ready for a night out', 'https://i.pinimg.com/564x/da/f2/d2/daf2d2e6c49d6b42af9b9f4e4f49c4e3.jpg', 4),
('pose idea', 'Strike a pose', 'https://i.pinimg.com/564x/22/47/7c/22477c036a7d772f3b3c7143f58b0bc1.jpg', 5),
('acoustic', 'My favorite artist', 'https://i.pinimg.com/564x/0d/8d/5b/0d8d5ba7e193bf6108c1e94a455e39e5.jpg', 1),
('cat', 'Too cute to handle', 'https://i.pinimg.com/564x/54/91/23/549123239d6748ab0d7f3e0b3c09b1f8.jpg', 2),
('running', 'Cant stop wont stop', 'https://i.pinimg.com/564x/58/30/04/58300461a39f79ea5127f8fa26c1a141.jpg', 3),
('quotes', 'Dream big', 'https://i.pinimg.com/564x/80/4b/d4/804bd4ad4e9e839f73a8d6d26b3f8803.jpg', 4),
('ootd', 'Casual chic', 'https://i.pinimg.com/564x/b7/61/80/b7618070a6222d4e', 5);

INSERT into pin (title, description, image_url, user_id)
VALUES
('quotes', 'Life is a journey, and if you fall in love with the journey, you will be in love forever.', 'https://i.pinimg.com/564x/0f/f8/68/0ff8685d5f5b5c2827648f870cdadfc4.jpg', 2),
('pose idea', 'Get some inspiration for your next photo session with these creative and unique pose ideas!', 'https://i.pinimg.com/564x/bd/c3/3d/bdc33d680ccf7d41b24427191b7e6ca1.jpg', 3),
('ootd', 'Classy never goes out of style.', 'https://i.pinimg.com/564x/3b/30/51/3b3051a6f92df8b7b6d662a0f7ad2e07.jpg', 4),
('running', 'Run like there is a hot guy in front of you and a creepy dude behind you!', 'https://i.pinimg.com/564x/51/06/ef/5106efc65b3f43010aeb40cc68241657.jpg', 5),
('cat', 'Adorable cat with beautiful blue eyes', 'https://i.pinimg.com/564x/19/f7/12/19f712609d7c2a9a3502eb2d2b1f31d5.jpg', 1),
('acoustic', 'Music is not just a sound. It is an emotion, a passion, a way of life', 'https://i.pinimg.com/564x/34/e8/4d/34e84dd4e7d25989c8f41f2bdc28d6fa.jpg', 2),
('quotes', 'Be so good they cant ignore you.', 'https://i.pinimg.com/564x/4a/3d/b3/4a3db38f7ce58cc5c2eaf7c8c7765eb5.jpg', 3),
('ootd', 'Be yourself, everyone else is already taken.', 'https://i.pinimg.com/564x/5c/56/10/5c5610e5f005e5e1d5fa05a4fb88ebad.jpg', 4),
('cat', 'Cute little kitty playing with a ball of yarn', 'https://i.pinimg.com/564x/19/33/2f/19332fa35bde1ee6c9d6b06dd6d232e6.jpg', 5),
('pose idea', 'Capturing the beauty of the moment with a creative pose idea', 'https://i.pinimg.com/564x/84/03/2c/84032c33b59d9f82c7316ec9d6d81306.jpg', 1),
('running', 'The only bad workout is the one that didnt happen', 'https://i.pinimg.com/564x/ee/f2/8c/eef28c82e62b587981a0bbf8b7c01d9a.jpg', 2),
('quotes', 'Dont wait for opportunities, create them', 'https://i.pinimg.com/564x/29/1f/7e/291f7e8c7f4d4474e4f3b70bf8a75c2a.jpg', 3);
