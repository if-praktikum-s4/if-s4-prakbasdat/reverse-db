# Reverse Engineer DB : Pinterest
**Pinterest** dipilih dari gabungan dari dua kata yaitu "pin" dan "interest". Pendirinya, Ben Silbermann, Paul Sciarra, dan Evan Sharp. Mereka menginginkan nama yang mencerminkan konsep inti platform tentang pengguna yang menyematkan dan mengatur konten berdasarkan minat mereka. Biasanya aplikasi ini digunakan untuk menemukan dan membagikan inspirasi

# 1st.

## Database Diagram
Database diagram Pinterest menampilkan struktur dasar yang mendukung fungsionalitas platform. Pada intinya, database Pinterest terdiri dari beberapa entitas utama, termasuk User, Pin, Board, dan Category. Pinterest secara efisien mengelola dan mengambil data, memungkinkan pengguna untuk menemukan, menyimpan, dan membagikan interest/ minat mereka dengan mudah.

### Diagram with dbdiagram
![diagram](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/raw/main/img/pinterest-dbdiagram.png)

### Diagram result in Dbeaver
![dbeaver](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/raw/main/img/pinterest-dbeaver.png)

Database : **MySQL**

Live design with : [dbdiagram](https://dbdiagram.io/d/645a4380dca9fb07c4c4693b)

# 2nd.

## DDL (Data Definition language)
Bagian dari SQL yang digunakan untuk mendefinisikan, mengubah, dan menghapus struktur objek database seperti tabel, view, indeks, dan sebagainya.

```sql
CREATE TABLE `users` (
  `user_id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) UNIQUE NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_picture` varchar(255)
);

CREATE TABLE `pins` (
  `pin_id` int PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `image_url` varchar(255) NOT NULL,
  `user_id` int NOT NULL
);

CREATE TABLE `boards` (
  `board_id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `user_id` int NOT NULL
);

```

For more DDL : [Script](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-ddl.sql)
# 3rd.
## DML (Data Manipulation language)

Bahasa pemrograman yang digunakan untuk memanipulasi data dalam sebuah database. Di MySQL, DML meliputi perintah-perintah seperti SELECT, INSERT, UPDATE, dan DELETE

### User Point of View 
Mencakup Data Manipulation Language (DML)



### INSERT
```sql
INSERT INTO User(name, nickname, email, password)
VALUES
('Rika Wijayanti', '@rikawijayanti', 'rikawijayanti@gmail.com', 'p4ssw0rd'),
('Dian Maulana', '@dianmaulana', 'dianmaulana@yahoo.com', 'password123'),
('Ivan Pratama', '@ivanpratama', 'ivanpratama@gmail.com', 'qwertyuiop');

INSERT INTO category(name)
VALUES
    ('Food'),
    ('Travel'),
    ('Fashion'),
    ('DIY'),
    ('Fitness'),
    ('Art'),
    ('Home Decor'),
    ('Wedding'),
    ('Quotes'),
```

### UPDATE
```sql
update user
set profile_picture = 'https://i.pinimg.com/564x/84/bd/42/84bd42e10f0d6b029adfc7e1005e9136.jpg'
where name = 'Rika Wijayanti' ;
```
### DELETE
```sql

DELETE from pin
WHERE description = 'Get some inspiration for your next photo session with these creative and unique pose ideas!';
```

For more DML:

1. [Boards](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-board.sql)
2. [Categories](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-category.sql)
3. [Comment](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-comment.sql)
4. [conversations](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-conversation.sql)
5. [Follows](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-follow.sql)
6. [Message](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-message.sql)
7. [Message 2](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-message2.sql)
8. [Pins](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-pin.sql)
9. [users](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dml-user.sql)


# 4th.
## DQL (Data Querying language)

DQL Statement digunakan untuk melakukan query pada data dalam objek skema. Tujuannya adalah untuk mendapatkan beberapa hubungan skema berdasarkan query yang diteruskan ke sana

### User Point of View

| No. | Analysis Question                                                                                                                     | Status    |
| --- | ------------------------------------------------------------------------------------------------------------------------------------- | --------- |
| 1   | Berapa banyak pin yang telah saya simpan dalam setiap papan (board) saya?                                                             | Done      |
| 2   | Bagaimana total jumlah pin yang saya simpan berkembang dari bulan ke bulan?                                                           | Done      |
| 3   | Apa pin paling populer yang saya miliki berdasarkan jumlah likes atau repins?                                                         | Done      |
| 4   | Bagaimana tingkat keterlibatan (engagement) pengguna terhadap pin-pin saya?                                                           | Done      |
| 5   | Apa pin yang paling sering dilihat atau dikomentari oleh pengguna lain?                                                               | Done      |
| 6   | Bagaimana sebaran kategori pin yang saya simpan dalam akun Pinterest saya?                                                            | On Hold   |
| 7   | Apakah ada pola atau tren tertentu dalam pin yang saya simpan berdasarkan waktu atau musim?                                           | Cancelled |
| 8   | Bagaimana rata-rata jumlah repin yang saya dapatkan per pin?                                                                          | Cancelled |
| 9   | Bisakah saya melihat statistik atau metrik kinerja pin saya dalam hal pertumbuhan pengikut atau interaksi?                            | Done      |
| 10  | Bagaimana perbandingan antara jumlah pin yang saya simpan dan jumlah pin yang saya buat sendiri?                                      | Cancelled |
| 11  | Apakah ada korelasi antara jumlah likes dan jumlah repins pada pin-pin saya?                                                          | Cancelled |
| 12  | Bagaimana perbandingan jumlah followers (pengikut) antara papan (board) saya satu dengan yang lain?                                   | Cancelled |
| 13  | Berapa jumlah keseluruhan likes yang diterima oleh pin-pi saya dalam satu bulan terakhir?                                             |           |
| 14  | Apa papan (board) yang paling banyak dikunjungi atau diakses oleh pengguna lain?                                                      |           |
| 15  | Bagaimana rasio repin-to-like untuk pin-pi saya dibandingkan dengan rata-rata pengguna Pinterest lainnya?                             |           |
| 16  | Apa perbedaan tingkat keterlibatan (engagement) pengguna pada pin-pi saya berdasarkan kategori?                                       |           |
| 17  | Bagaimana jumlah komentar pada pin-pi saya berubah seiring waktu?                                                                     |           |
| 18  | Apakah ada tren perubahan jumlah pengikut pada akun Pinterest saya dari bulan ke bulan?                                               |           |
| 19  | Bagaimana perbandingan jumlah repins antara pin-pi saya yang memiliki deskripsi dengan yang tidak memiliki deskripsi?                 |           |
| 20  | Apakah ada perbedaan interaksi pengguna berdasarkan gambar sampul (cover image) papan (board) saya?                                   |           |
| 21  | Bagaimana sebaran geografis pengikut saya di Pinterest?                                                                               |           |
| 22  | Apakah ada pola harian atau jam-jam tertentu di mana pin-pi saya mendapatkan tingkat interaksi yang lebih tinggi?                     |           |
| 23  | Berapa persentase pin-pin saya yang diakses melalui pencarian (search) daripada melalui tautan langsung (direct link)?                |           |
| 24  | Apa perbedaan keterlibatan (engagement) pengguna antara pin-pi yang saya simpan sendiri dengan yang saya repin dari pengguna lain?    |           |
| 25  | Bagaimana perubahan jumlah komentar pada pin-pi saya sebelum dan setelah saya melakukan promosi atau iklan terkait?                   |           |
| 26  | Apakah ada pin-pi yang memiliki tingkat keterlibatan (engagement) yang rendah dan memerlukan pembaruan atau penyempurnaan?            |           |
| 27  | Bagaimana jumlah pin-pi saya berbanding dengan jumlah pin yang saya repin dari pengguna lain?                                         |           |
| 28  | Apa perbandingan jumlah pengguna yang menyimpan pin-pi saya dengan pengguna yang repin pin-pi saya?                                   |           |
| 29  | Bagaimana perubahan jumlah pengguna baru yang mengikuti akun Pinterest saya dari waktu ke waktu?                                      |           |
| 30  | Apakah ada perbedaan keterlibatan (engagement) pengguna antara pin-pi saya yang memiliki teks pada gambar dengan yang tidak memiliki? |           |
| 31  | Bagaimana cara saya menyimpan pin atau gambar favorit ke dalam koleksi saya?                                                          |           |
| 32  | Apa langkah-langkah untuk membuat papan (board) baru dan menambahkan pin ke dalamnya?                                                 |           |
| 33  | Bagaimana cara saya menghapus pin yang tidak lagi saya inginkan?                                                                      |           |
| 34  | Bisakah saya mengedit deskripsi atau detail lainnya pada pin yang sudah saya simpan sebelumnya?                                       | Cancelled |
| 35  | Bagaimana cara saya mengubah urutan pin di papan saya?                                                                                | Done      |
| 36  | Apa langkah-langkah untuk membuat komentar pada pin yang saya sukai?                                                                  | Cancelled |
| 37  | Bagaimana cara saya menandai atau membagikan pin ke pengguna Pinterest lainnya?                                                       | Cancelled |
| 38  | Apakah ada batasan jumlah pin yang dapat saya simpan dalam koleksi atau papan saya?                                                   | On Hold   |
| 39  | Bagaimana saya bisa mencari pin berdasarkan kategori atau kata kunci tertentu?                                                        | Done      |
| 40  | Bisakah saya membuat pin baru langsung dari aplikasi mobile Pinterest?                                                                | Cancelled |

### CEO / Management Point of view


| No. | Analysis Question                                                                                                                                     | Status |
| --- | ----------------------------------------------------------------------------------------------------------------------------------------------------- | ------ |
| 1   | Berapa total jumlah pengguna terdaftar di Pinterest saat ini?                                                                                         |        |
| 2   | Bagaimana tren pertumbuhan jumlah pengguna baru dalam 6 bulan terakhir?                                                                               |        |
| 3   | Apa usia rata-rata pengguna Pinterest?                                                                                                                |        |
| 4   | Berapa persentase pengguna Pinterest yang aktif secara harian?                                                                                        |        |
| 5   | Apakah ada perbedaan penggunaan Pinterest antara pengguna pria dan wanita?                                                                            |        |
| 6   | Bagaimana sebaran geografis pengguna Pinterest di berbagai negara?                                                                                    |        |
| 7   | Berapa rata-rata jumlah pin yang disimpan oleh pengguna dalam satu bulan?                                                                             |        |
| 8   | Apa kategori pin yang paling populer di Pinterest?                                                                                                    |        |
| 9   | Apakah ada tren perubahan popularitas kategori pin dari waktu ke waktu?                                                                               |        |
| 10  | Bagaimana jumlah repins rata-rata per pin dalam kategori tertentu?                                                                                    |        |
| 11  | Apa pin yang memiliki jumlah repins tertinggi sepanjang sejarah Pinterest?                                                                            |        |
| 12  | Berapa persentase pengguna Pinterest yang mengakses platform melalui aplikasi mobile?                                                                 |        |
| 13  | Apa perbandingan jumlah pengguna yang mengakses Pinterest melalui perangkat desktop dan mobile?                                                       |        |
| 14  | Bagaimana tingkat keterlibatan (engagement) pengguna dengan iklan atau konten promosi di Pinterest?                                                   |        |
| 15  | Apakah ada perbedaan tingkat keterlibatan (engagement) pengguna antara iklan berbayar dan konten organik di Pinterest?                                |        |
| 16  | Bagaimana rasio klik (click-through rate) untuk iklan yang ditampilkan di Pinterest?                                                                  |        |
| 17  | Apa kata kunci yang paling banyak dicari oleh pengguna dalam pencarian Pinterest?                                                                     |        |
| 18  | Bagaimana sebaran jenis perangkat yang digunakan pengguna untuk mengakses Pinterest (misalnya, smartphone, tablet, desktop)?                          |        |
| 19  | Berapa jumlah keseluruhan likes yang diberikan oleh pengguna Pinterest dalam sebulan terakhir?                                                        |        |
| 20  | Apakah ada perubahan pola penggunaan Pinterest pada hari-hari libur atau peristiwa khusus?                                                            |        |
| 21  | Bagaimana perbandingan jumlah pin yang diunggah oleh pengguna sendiri dengan pin yang diunggah oleh pengguna lain?                                    |        |
| 22  | Apa jenis konten visual yang paling sering diunggah oleh pengguna di Pinterest?                                                                       |        |
| 23  | Berapa jumlah rata-rata repins yang diterima oleh pin dalam 24 jam pertama setelah diunggah?                                                          |        |
| 24  | Apakah ada perbedaan jumlah repins antara pin yang memiliki deskripsi panjang dan pendek?                                                             |        |
| 25  | Bagaimana perbandingan jumlah komentar pada pin yang berhubungan dengan produk perusahaan dengan pin yang tidak berhubungan dengan produk perusahaan? |        |
| 26  | Apakah ada pin yang memiliki tingkat keterlibatan (engagement) yang tinggi dan berpotensi menjadi konten viral?                                       |        |
| 27  | Bagaimana jumlah followers (pengikut) rata-rata perusahaan di Pinterest dibandingkan dengan pesaing utama?                                            |        |
| 28  | Apa perbandingan jumlah pengguna yang mengunjungi profil perusahaan di Pinterest dengan jumlah pengikut perusahaan?                                   |        |
| 29  | Berapa persentase pengguna Pinterest yang berasal dari referensi atau rujukan dari platform lain?                                                     |        |
| 30  | Apakah ada tren perubahan rasio pengguna aktif perusahaan terhadap jumlah pengikut perusahaan?                                                        |        |
| 31  | Bagaimana tingkat keterlibatan (engagement) pengguna terhadap papan (board) perusahaan di Pinterest?                                                  |        |
| 32  | Apa perbandingan jumlah repins untuk konten asli perusahaan dibandingkan dengan konten yang di-repin dari pengguna lain?                              |        |
| 33  | Bagaimana perubahan jumlah pengunjung situs web perusahaan yang berasal dari tautan di Pinterest?                                                     |        |
| 34  | Apakah ada perbedaan tingkat keterlibatan (engagement) pengguna antara pengguna baru dan pengguna yang sudah lama menggunakan Pinterest?              |        |
| 35  | Bagaimana sebaran demografi pengguna yang paling aktif berinteraksi dengan konten perusahaan di Pinterest?                                            |        |
| 36  | Berapa persentase pengguna yang melakukan pembelian produk setelah melihat pin perusahaan di Pinterest?                                               |        |
| 37  | Apakah ada perbedaan konversi penjualan antara pengguna yang menemukan produk perusahaan melalui pin organik dan pin berbayar?                        |        |
| 38  | Bagaimana sebaran waktu kunjungan pengguna perusahaan di Pinterest (misalnya, pagi, siang, malam)?                                                    |        |
| 39  | Berapa persentase pengguna Pinterest yang menggunakan fitur "Shop" untuk berbelanja langsung dari platform?                                           |        |
| 40  | Apakah ada perbedaan keterlibatan (engagement) pengguna antara pengguna Pinterest yang mengikuti akun perusahaan dan yang tidak mengikuti?            |        |

For more DQL :
1. [Basic](https://gitlab.com/if-praktikum-s4/if-s4-prakbasdat/reverse-db/-/blob/main/sql/job-interview-dql.sql)



# 5th.
## Youtube Documentation
Demo Reverse Engineering : [via YouTube]()
